import _get from 'lodash.get'
import debug from 'debug';
import superagent from 'superagent';
import superagentPromise from 'superagent-promise';
import join from 'url-join';

import repo from './repo';
import snippet from './snippet';
import team from './team';
import user from './user';

const log = debug('bitbucketjs:bitbucket');

module.exports = function (opts={}) {
  opts.Promise = opts.Promise || Promise;
  opts.apiRoot = opts.apiRoot || 'https://api.bitbucket.org/';
  opts.apiRoot = join(opts.apiRoot, '/');


  const oldEnd = superagent.Request.prototype.end;
  superagent.Request.prototype.end = function (cb) {
    log('requesting', this.method, ':', this.url);

    if (opts.username && opts.password) {
      this.auth(opts.username, opts.password);
      this._authenticated = true;
    }

    return oldEnd.call(this, function (err, res) {
      // if the response has a parsed body payload, return that instead
      if (res && res.body) {
        res = res.body;
      };

      // if the error has a parsed error message in the body, return that instead
      const bodyMessage = _get(err, 'response.body.error.message');
      if (bodyMessage) {
        err.message = bodyMessage;
      }

      cb.call(this, err, res);
    })
  }

  const agent = superagentPromise(superagent, opts.Promise);
  if (opts.username && opts.password) {
    agent._bb_authenticated = true;
  }

  agent._Promise = opts.Promise;

  opts.request = agent;

  return {
    request: agent,
    repo: repo(opts),
    user: user(opts),
    snippet: snippet(opts),
    team: team(opts)
  }
}
