import nock from 'nock';
import url from 'url';
import { config } from './util';

const nockback = nock.back;
const hostname = url.parse(config.apiRoot).hostname

nock.enableNetConnect(hostname);
nockback.fixtures = __dirname + '/nock_fixtures';

function nockbackP(path, promise) {
  return new Promise(function (resolve, reject) {
    nockback(path, function (done) {
      promise.then(resolve, reject);
      promise.then(done, done);
    });
  });
}

export default nockbackP;
