import { expect } from './chai';
import { getClient, config } from './util';

import sinon from 'sinon';
import join from 'url-join';
import nockback from './nockback';

describe('bitbucket.repo', () => {
  describe('fetching a repo from slug', () => {
    it('returns the matching repository', () => {
      let bitbucket = getClient();
      sinon.spy(bitbucket.request, 'get');

      return nockback(`repo/atlassian/aui.json`,
        bitbucket.repo.fetch('atlassian/aui')
          .then(repo => {
            expect(repo.name).to.equal('aui');
            expect(repo.owner.username).to.equal('atlassian');
            expect(bitbucket.request.get).to.have.been.calledWith(join(config.apiRoot, '2.0', 'repositories', 'atlassian', 'aui'));
          })
      );
    })
  })

  describe('fetching a repo from owner', () => {
    it('returns the user\'s repositories', () => {
      let bitbucket = getClient();
      sinon.spy(bitbucket.request, 'get');

      return nockback('repo/atlassian.json',
        bitbucket.repo.forOwner('atlassian')
          .then(repos => {
            expect(bitbucket.request.get).to.have.been.calledWith(join(config.apiRoot, '2.0', 'repositories', 'atlassian'));
            expect(repos.values).to.be.instanceof(Array);
          })
      );
    })
  })

  describe('creating a repo', () => {
    let bitbucket;

    beforeEach(() => {
      bitbucket = getClient();
      sinon.spy(bitbucket.request, 'post');
    })

    it('understands username/reponame', () => {
      return nockback(`repo/${config.username}/created.post.json`,
        bitbucket.repo.create(`${config.username}/created`).then(() => {
          return bitbucket.repo.fetch(`${config.username}/created`)
        }).then((repo) => {
          expect(repo.name).to.equal('created');
          expect(repo.owner.username).to.equal(config.username);
        })
      );
    });

    afterEach(() => {
      return nockback(`repo/${config.username}/created.post.after.json`,
        bitbucket.repo.delete(`${config.username}/created`)
      );
    })
  })

  describe('deleting a repo', () => {
    let bitbucket;

    beforeEach(() => {
      bitbucket = getClient();
      return nockback(`repo/${config.username}/repo.delete.before.json`,
        bitbucket.repo.create(`${config.username}/todelete`)
      )
    })

    it('sends a DELETE to the repo endpoint', () => {
      return nockback(`repo/${config.username}/repo.delete.json`,
        expect(bitbucket.repo.delete(`${config.username}/todelete`).then(() => {
          return bitbucket.repo.fetch(`${config.username}/todelete`);
        })).to.be.rejected);
    })
  })
})
